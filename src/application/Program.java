package application;

import java.util.Locale;
import java.util.Scanner;

import entities.Triangle;

public class Program {

	public static void main(String[] args) {
		
	Locale.setDefault(Locale.US);
	Scanner sc = new Scanner(System.in);
	
	Triangle x, y;
	
	x = new Triangle();
	y = new Triangle();
	
	System.out.println("Insira os tres valores das dimensoes do triangulo x: ");
	x.a = sc.nextDouble();
	x.b = sc.nextDouble();
	x.c = sc.nextDouble();
	
	System.out.println("Insira os tres valores das dimensoes do triangulo y: ");
	y.a = sc.nextDouble();
	y.b = sc.nextDouble();
	y.c = sc.nextDouble();
	
	double areax = x.area();
	double areay = y.area();
		
	System.out.printf("A area do triangulo x eh: %.2f%n", areax);
	System.out.printf("A area do triangulo Y eh: %.2f%n", areay);
	
	if (areax == areay) {
		System.out.println("A area dos triangulos sao iguais");
	}
	else if (areax > areay) {
		System.out.println("A area do triangulo X eh maior que do triangulo Y");
		}
		else {
	System.out.println("A area do triangulo Y eh maior que do triangulo X");
}
		sc.close();
		
	}

}
